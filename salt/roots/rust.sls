rustup: 
    pkg.installed:
        - name: rustup
    cmd.run:
        - name: rustup-init -y -q
        - runas: jelemux
    require:
        - user.present: userconfig

{% for toolchain in ['stable', 'nightly'] %}
rust_{{ toolchain }}:
    cmd.run:
        - name: rustup toolchain install {{ toolchain }}
        - runas: jelemux
    require:
        - pkg: rustup
        - cmd: rustup
{% endfor %}

rust_stable_default:
    cmd.run:
        - name: rustup default stable
        - runas: jelemux
    require:
        - cmd: rust_stable

{% for package in ['wasm-pack', 'wasm-gc', 'cargo-generate', 'cargo-make'] %}
{{ package }}:
    cmd.run:
        - name: cargo install {{ package }}
        - runas: jelemux
    require:
        - cmd: rust_stable
{% endfor %}

# todo: add $HOME/.cargo/bin to PATH