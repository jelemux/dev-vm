tools:
    pkg.installed:
        - pkgs:
            - build-base
            - openssl
            - openssl-dev
            - wget
            - curl
            - sed
            - tree
            - htop
            - git
            - fish
            - neovim
            - emacs

date > /tmp/date:
    cmd:
        - run

{% set username = 'jelemux' %}
{% set usergroup = username %}
{% set home_dir = '/home/jelemux' %}
{% set shell = '/usr/bin/fish' %}
{% set groups = ['wheel', 'lp'] %}

{% set list_users = salt.user.list_users() %}
userconfig:
{% if grains['os_family'] == 'Alpine' %}
    {% if username is match(list_users) %} 
    cmd.run:
        - name: adduser -h {{ home_dir }} -s {{ shell }} -G {{ usergroup }} -D {{ username }}
    {% for group in groups %}
    cmd.run:
        - name: adduser {{ username }} {{ group }}
    {% endfor %}
    {% endif %}
{% else %}
    user.present: # doesn't work because alpine uses adduser, not useradd
        - name: {{ username }}
        - shell: {{ shell }}
        - home: {{ home_dir }}
        - groups:
        {% for group in groups %}
            - {{ group }}
        {% endfor %}
        - password: 5c53f94e88f9a105271e9fd485a3f427e5ea19aae0cf5bd13bef8c51327a5d83
{% endif %}
